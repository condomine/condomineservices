package br.com.condomine.constants;

public enum ServicesStatus {

	SUCCES(1),
	FAIL(2),
	WAIT(3);

	Integer value;

	ServicesStatus(Integer value){
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
}
