package br.com.condomine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import br.com.condomine.constants.ServicesStatus;
import br.com.condomine.modelo.Usuario;
import br.com.condomine.repository.UsuarioRepository;
import br.com.condomine.service.UsuarioService;

@RestController
public class UsuarioController {

	@Autowired
	private UsuarioService service;

	@Autowired
	private UsuarioRepository usuarioRepository;

	public Integer criaUsuario(Usuario usuario){
		try{
			usuarioRepository.save(usuario);
			return ServicesStatus.SUCCES.getValue();
		}catch(Exception e){
			e.printStackTrace();
			return ServicesStatus.FAIL.getValue();
		}

	}

	public Integer editaUsuario(Usuario usuario){
		return ServicesStatus.SUCCES.getValue();
	}

	public Integer deletaUsuario(Long id){
		try{
			usuarioRepository.delete(id);
			return ServicesStatus.SUCCES.getValue();
		}catch(Exception e){
			e.printStackTrace();
			return ServicesStatus.FAIL.getValue();
		}
	}
	public Usuario buscaUsuario(Long id){
		try{
			return usuarioRepository.findById(id);
		}catch(Exception e){
			e.printStackTrace();
			return new Usuario();
		}
	}

	public UsuarioService getService() {
		return service;
	}

	public void setService(UsuarioService service) {
		this.service = service;
	}

	public UsuarioRepository getUsuarioRepository() {
		return usuarioRepository;
	}

	public void setUsuarioRepository(UsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}
}
