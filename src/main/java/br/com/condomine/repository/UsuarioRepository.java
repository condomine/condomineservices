package br.com.condomine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.condomine.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	Usuario findById(Long id);
}
