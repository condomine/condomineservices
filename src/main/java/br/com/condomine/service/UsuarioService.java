package br.com.condomine.service;

import br.com.condomine.modelo.Usuario;

public interface UsuarioService {
	String create(Usuario usuario);
	String edit(Usuario usuario);
	String delete(Integer id);
	String get(Usuario usuario);

}
