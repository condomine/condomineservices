package br.com.condomine.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.condomine.Application;
import br.com.condomine.constants.ServicesStatus;
import br.com.condomine.controller.UsuarioController;
import br.com.condomine.modelo.Usuario;
import br.com.condomine.modelo.UsuarioPK;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration

public class UsuarioTests {
	private static Usuario usuario;
	@Autowired
	UsuarioController controller;
	
	@BeforeClass
	public static void initObjects(){
		usuario = new Usuario();
		usuario.setId(new UsuarioPK());
	}
	
	

	@Test
	public void criaUsuarioTest(){
		usuario.setApelido("Ana");
		usuario.getId().setEmail("ana.card.rodrigues@gmail.com");
		usuario.setNome("Ana Paula Rodrigues Corrêa");
		assertSame(controller.criaUsuario(usuario), ServicesStatus.SUCCES.getValue());
	}

	@Test
	public void editaUsuarioTest(){
		usuario.setApelido("Aninha");
		usuario.getId().setId(5L);
		controller.criaUsuario(usuario);
		assertSame(controller.editaUsuario(usuario), ServicesStatus.SUCCES.getValue());
	}

	@Test
	public void deletaUsuarioTest(){
//		controller.deletaUsuario(2L);
	}

	@Test
	public void buscaUsuario(){
		Usuario user3 = controller.buscaUsuario(3L);
		assertEquals(user3.getApelido(), "Ana");
		
	}
}
